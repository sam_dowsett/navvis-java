package com.sam.validation;

import java.util.Arrays;

public class LastArgumentIsATextFile implements ArgumentValidator{

    @Override
    public boolean isValid(String[] args) {
        return Arrays.stream(args).allMatch(s -> s.endsWith(".txt"));
    }

    @Override
    public String getErrorMessage() {
        return "supplied argument must be a relitive path txt file";
    }

}
