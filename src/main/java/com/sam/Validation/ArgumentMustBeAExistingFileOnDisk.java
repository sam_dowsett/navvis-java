package com.sam.validation;

import java.util.Arrays;

import com.sam.utils.FileSystem;

public class ArgumentMustBeAExistingFileOnDisk implements ArgumentValidator {

    private final FileSystem fileSystem;

    public ArgumentMustBeAExistingFileOnDisk(FileSystem fileSystem) {
        this.fileSystem = fileSystem;
    }

    @Override
    public boolean isValid(String[] args) {
        return Arrays.stream(args).allMatch(f -> fileSystem.isFile(f));
    }

    @Override
    public String getErrorMessage() {
        return "File not found";
    }
}