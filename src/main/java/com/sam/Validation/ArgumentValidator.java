package com.sam.validation;

public interface ArgumentValidator{

    boolean isValid(String[] args);
    String getErrorMessage();
}
