package com.sam.validation;

public class OnlyOneArgument implements ArgumentValidator{

    @Override
    public boolean isValid(String[] args) {
        return args.length == 1;
    }

    @Override
    public String getErrorMessage() {
        return "You may only specify one terget file";
    }
}
