package com.sam.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class FileSystem {

    public Stream<String> getLines(String fileName) throws IOException{
        return Files.lines(getPath(fileName));
    }

    public boolean isFile(String fileName){
        return Files.exists(getPath(fileName));
        
    }

    private Path getPath(String fileName){
        return Paths.get(fileName);
    }
}