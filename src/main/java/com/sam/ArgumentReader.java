package com.sam;

import com.sam.validation.ArgumentValidator;

import java.util.Collection;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ArgumentReader {

    private Stream<ArgumentValidator> validators;

    public ArgumentReader(Stream<ArgumentValidator> validators) {
        this.validators = validators;
    }

    public String readArguments(String[] args) {
        Collection<String> failedValidations = validators.filter(val -> !val.isValid(args))
                .map(ArgumentValidator::getErrorMessage).collect(Collectors.toSet());
        if (!failedValidations.isEmpty()) {
            throw new IllegalArgumentException(String.join("\n", failedValidations));
        }

        // validations are passed so the first value in our arguments is our filename;
        return args[0];
    }
}
