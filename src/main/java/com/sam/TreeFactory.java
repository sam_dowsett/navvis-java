package com.sam;

import java.util.*;
import java.util.stream.Collectors;
import com.sam.model.*;

public class TreeFactory {

    public TreeNode buildTree(List<String> words) {
        //spring @NotNull would protect us from this 
        if(words == null || words.isEmpty()){
            return TreeNode.Empty();
        }
       return buildTreeFromLeaves(getLeaves(words));
    }    

    private TreeNode buildTreeFromLeaves(List<TreeNode> leaves){
        if(leaves.size() == 1){
            return leaves.get(0);
        }
        
        TreeNode parentNode = getParentNode(leaves);
        List<TreeNode> remaining = leaves.stream().skip(2).collect(Collectors.toList());    
        return buildTreeFromLeaves(addOrdered(parentNode,remaining));
    }

    private TreeNode getParentNode(List<TreeNode> leaves) {
        TreeNode left = leaves.get(0);
        TreeNode right = leaves.get(1);

        TreeNode newNode = new TreeNode(
                new NodeValue(String.format("%s %s", left.getValue().getText(), right.getValue().getText()),
                        left.getValue().getCount() + right.getValue().getCount()),
                left, right);
        return newNode;
    }

    //list does not allow sepcification of a comparitor for get index. 
    private List<TreeNode> addOrdered(TreeNode toInsert, List<TreeNode> nodes){
        if(nodes.isEmpty() || toInsert.getValue().getCount() >= nodes.get(nodes.size()-1).getValue().getCount()){
            nodes.add(toInsert);
            return nodes;
        }

        int target = 0;
        boolean positionFound = false;  
        Iterator<TreeNode> itr = nodes.iterator();
        while (itr.hasNext()&& !positionFound){
            TreeNode curTreeNode = itr.next();
            if (curTreeNode.getValue().getCount() > toInsert.getValue().getCount()){
                positionFound = true;
            } else{
                target = target+1;
            }
        }

        nodes.add(target, toInsert);
        return nodes;
    }

    private List<TreeNode> getLeaves(List<String> words){
        return words.stream()
        .collect(Collectors.toMap(w -> w, w -> 1, Integer::sum))
        .entrySet()
        .stream()
        .sorted(Map.Entry.comparingByValue())
        .map(i -> new TreeNode(new NodeValue(i.getKey(), i.getValue()), null,null))
        .collect(Collectors.toList());
    }
}