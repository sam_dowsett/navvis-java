package com.sam;

/**
 * Hello world!
 */
public final class App {

    private App() {
    }

    /**
     * Says hello to the world.
     * @param args The arguments of the program.
     */
    public static void main(String[] args) {

        Application application = new Application();
        try {
            System.out.println(application.run(args));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
