package com.sam.model;

public class TreeNode {

    private TreeNode left;
    private TreeNode right;
    private NodeValue value;

    public TreeNode(NodeValue value, TreeNode left, TreeNode right){
        this.value = value;
        this.left = left;
        this.right = right;
    }

	public NodeValue getValue(){
        return this.value;
    }

    public TreeNode getLeft(){
        return left;
    }

    public TreeNode getRight(){
        return right;
    }

    public boolean isLeaf(){
        return right == null && left == null;
    }

    public boolean isEmpty(){
        return right == null && left == null && this.getValue().isEmpty();
    }

    public String toString(){
        return String.format("%s: %s", this.value.getText(), this.value.getCount());
    }

    public static TreeNode Empty(){
        return new TreeNode(NodeValue.Empty(), null, null);
    }
}