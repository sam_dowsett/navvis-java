package com.sam.model;

public class NodeValue {

    private String text;

    private int count;

    public NodeValue(String text, int count){
        this.text = text;
        this.count = count;
    }

    public String getText() {
        return text;
    }

    public int getCount() {
        return count;
    }

    public static NodeValue Empty(){
        return new NodeValue("", 0);
    }

	public boolean isEmpty() {
		return count == 0 || text == ""; 
	}
}