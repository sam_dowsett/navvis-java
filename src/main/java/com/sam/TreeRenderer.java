package com.sam;

import java.util.Collections;

import com.sam.model.TreeNode;

public class TreeRenderer {
    public String render(TreeNode root){
        StringBuilder out = new StringBuilder();
        return render(root, out, 0).toString();
    }

    private StringBuilder render(TreeNode node, StringBuilder out, int depth){
        if (node == null || node.isEmpty()){
            return out;
        }
        depth = depth + 3;
        render(node.getRight(), out, depth);
        
        out.append(System.lineSeparator());
        out.append(String.join("", Collections.nCopies(depth, " ")));
        out.append(String.format("%s%s", renderNodeValue(node), System.lineSeparator()));

        render(node.getLeft(), out, depth);
        return out;
    }

    private String renderNodeValue(TreeNode node){
        return node.isLeaf() ? node.toString() :
            String.format("%s", node.getValue().getCount()); 
    }
}