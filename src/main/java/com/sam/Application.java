package com.sam;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import com.sam.model.TreeNode;
import com.sam.utils.FileSystem;
import com.sam.validation.*;

//application entry point and dependancy configuration. 
//Use of a IOC container or framework such as spring would improve this
public class Application {

    private final FileSystem fileSystem;
    // Dependancies
    private final Stream<ArgumentValidator> argumentValidators;
    private final ArgumentReader argumentReader;
    private final FileReader fileReader;
    private final TreeFactory treeFactory;
    private final TreeRenderer renderer;

    public Application() {
        this.fileSystem = new FileSystem();
        this.argumentValidators = Arrays.stream(new ArgumentValidator[] { 
            new OnlyOneArgument(),
            new LastArgumentIsATextFile(), 
            new ArgumentMustBeAExistingFileOnDisk(fileSystem) });
        this.argumentReader = new ArgumentReader(argumentValidators);
        this.fileReader = new FileReader(fileSystem);
        this.treeFactory = new TreeFactory();
        this.renderer = new TreeRenderer();
    }

    public String run(String[] args) {
        List<String> words = fileReader.getWordsFrom(argumentReader.readArguments(args));
        TreeNode root = treeFactory.buildTree(words);
        return renderer.render(root);
    }
}