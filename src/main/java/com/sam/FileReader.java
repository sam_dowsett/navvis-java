package com.sam;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.*;

import com.sam.utils.FileSystem;

class FileReader {

    private final FileSystem fileSystem;

    public FileReader(FileSystem fileSystem){
        this.fileSystem = fileSystem;
    }

    public List<String> getWordsFrom(String fileName) {
        Stream<String> lines;
        try {
            lines = fileSystem.getLines(fileName);
            List<String> data = Arrays.stream(lines.collect(Collectors.joining(System.lineSeparator())).replaceAll("\\.|,", "").replaceAll("\\s+", " ").split(" ")).collect(Collectors.toList());
            lines.close();
            return data;
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }
}

