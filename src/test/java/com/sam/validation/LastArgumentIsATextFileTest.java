package com.sam.validation;

import org.junit.Test;

import static org.junit.Assert.*;
/**
 * Unit test for simple App.
 */
public class LastArgumentIsATextFileTest {
    
    private String[] one = {"file.txt"};

   
    private String[] invalid = {"one","two","three"}; 
    private ArgumentValidator classUnderTest;
    
    @Test
    public void a_non_txt_file_is_invalid() {
        //arange
         classUnderTest = new LastArgumentIsATextFile();

        //act
        boolean result = classUnderTest.isValid(invalid);
        //assert
        assertFalse(result);
    }

    @Test
    public void a_txt_file_is_valid() {
               //arange
               classUnderTest = new LastArgumentIsATextFile();

               //act
               boolean result = classUnderTest.isValid(one);
               //assert
               assertTrue(result);
    }

}