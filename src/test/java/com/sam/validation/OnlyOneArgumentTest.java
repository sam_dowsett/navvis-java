package com.sam.validation;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Unit test for simple App.
 */
public class OnlyOneArgumentTest {
    
    private String[] one = {"file.txt"};

    private String[] none ={};
    
    private String[] many = {"one","two","three"}; 
    private ArgumentValidator classUnderTest;
    
    @Test
    public void when_no_arguments_are_provided_arguments_are_invalid() {
        //arange
         classUnderTest = new OnlyOneArgument();

        //act
        boolean result = classUnderTest.isValid(none);
        //assert
        assertFalse(result);
    }

    @Test
    public void when_one_argument_is_provided_arguments_are_valid() {
               //arange
               classUnderTest = new OnlyOneArgument();

               //act
               boolean result = classUnderTest.isValid(one);
               //assert
               assertTrue(result);
    }

    @Test
    public void when_multiple_argumenst_is_provided_arguments_are_valid() {
               //arange
               classUnderTest = new OnlyOneArgument();

               //act
               boolean result = classUnderTest.isValid(many);
               //assert
               assertFalse(result);
    }
}