package com.sam;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.List;

import com.sam.model.TreeNode;

/**
 * Unit test for simple App.
 */
public class TreeFactoryTest {
    
    TreeFactory classUnderTest = new TreeFactory();
    
    @Test
    public void a_node_with_no_children_is_returned_for_one_word() {
        //arrange
        List<String> words = List.of("one", "one", "one");
        
        //act
        TreeNode result = classUnderTest.buildTree(words);

        //assert
        assertTrue(result.isLeaf());
    }

    @Test
    public void a_tree_with_children_is_returned() {
        //arrange
        List<String> words = List.of("one", "one", "two");
        
        //act
        TreeNode result = classUnderTest.buildTree(words);

        //assert
        assertFalse(result.isLeaf());
    }


    @Test
    public void a_tree_has_leaf_nodes_for_each_unique_word() {
        //arrange
        List<String> words = List.of("one", "one", "two", "two", "three","four","five","five");
        
        //act
        TreeNode result = classUnderTest.buildTree(words);

        //assert
        assertEquals(countLeafNodes(result), 5);
    }

    @Test
    public void an_empty_word_or_null_returns_a_empty_tree() {
        //arrange
        List<String> words =  Collections.emptyList();
        
        //act
        //assert
        assertEquals(countLeafNodes(classUnderTest.buildTree(words)), 1);
        assertEquals(countLeafNodes(classUnderTest.buildTree(null)), 1);
        
    }

    private int countLeafNodes(TreeNode root){
        if (root.isLeaf()){
            return 1;
        }

        return countLeafNodes(root.getLeft()) + countLeafNodes(root.getRight());
    } 
}
