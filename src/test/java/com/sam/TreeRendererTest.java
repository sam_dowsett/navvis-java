package com.sam;

import org.junit.Test;

import static org.junit.Assert.*;

import java.util.Collections;
import java.util.List;

import com.sam.model.TreeNode;

/**
 * Unit test for simple App.
 */
public class TreeRendererTest {
    
    TreeFactory builder = new TreeFactory();
    TreeRenderer classUnderTest = new TreeRenderer();

    @Test
    public void a_tree_renderered_on_one_line() {
        //arrange
        List<String> words = List.of("one", "one", "one");
        TreeNode tree = builder.buildTree(words);
        //act
        String result = classUnderTest.render(tree);


        //assert
        assertEquals(System.lineSeparator() + "   one: 3" + System.lineSeparator(), result);
    }

    @Test
    public void a_tree_with_children_is_lists_each_child() {
        //arrange
        List<String> words = List.of("one", "one", "two");
        TreeNode tree = builder.buildTree(words);
        
        //act
        String result = classUnderTest.render(tree);

        //assert
        assertEquals(result, "\n      one: 2\n\n   3\n\n      two: 1\n");
    }


    @Test
    public void an_empty_tree_is_not_rendered() {
        //arrange
        List<String> words =  Collections.emptyList();
        TreeNode tree = builder.buildTree(words);

        //act
        String result = classUnderTest.render(tree);

        //assert
        assertEquals("", result);
    }
}